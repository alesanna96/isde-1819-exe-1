from pandas import read_csv

import numpy as np

def split_data(X, y, tr_fraction=0.5):
    """Split the data X,y into two random subsets."""
    num_samples = y.size  # the total number of samples
    num_tr = int(num_samples * tr_fraction)  # computes the number of training samples
    num_ts = num_samples - num_tr  # computes the number of test samples

    idx = np.array(range(0, num_samples))  # An array with a number for each line of data matrix
    np.random.shuffle(idx)  # shuffles the idx array

    tr_idx = idx[0:num_tr]  # Tells us which are the indexes that will form datatr and indexests
    ts_idx = idx[num_tr:]  # Tells us which are the indexes that will form datats and indexests

    datatr = X[tr_idx, :]
    datats = X[ts_idx, :]
    indexestr = y[tr_idx]
    indexests = y[ts_idx]

    return datatr, indexestr, datats, indexests


def count_digits(y):
    """Count the number of elements in each class."""
    classes = np.unique(y)
    p = np.zeros(dtype=int, shape=(classes.size, ))
    for index, k in enumerate(classes):
        p[index] = int(np.sum(y == k, dtype=int))
    return p


def fit(Xtr, ytr):
    """Compute the average centroid for each class."""
    numPixels = Xtr.shape[1]
    numClasses = np.unique(ytr).size
    centroids = np.zeros(shape=(numClasses, numPixels))
    for index in xrange(numClasses):
        centroid_i = np.mean(Xtr[ytr == index], axis=0)
        assert centroid_i.size == numPixels
        centroids[index] = centroid_i
    return centroids


def load_data(filename):
    data = read_csv('./data/minst_data.csv')
    data = np.array(data)
    X = data[:, 1:]
    y = data[:, 0]
    return X, y
